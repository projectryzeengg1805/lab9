package ryze.ui;

import java.util.HashMap;
import java.util.List;

//added
import java.io.File;
import ryze.analytics.BuggyAnalyzer;
import ryze.analytics.BuggyAnalysis;
import ryze.billing.BuggyBiller;
import ryze.query.BuggyFileTextQuery;

import java.util.Scanner;
import java.util.HashMap;

public class BuggyRyzeCommandLineUI {

	public static void main(String[] args) throws Exception{
//added
		if(args.length == 0){
			System.out.println("Please enter an argument");
			// for eclipse, go to RUN > RUN CONFIGURATIONS > ARGUMENTS to set the arguments
			return;
		}

// changed
		BuggyFileTextQuery tq = new BuggyFileTextQuery();
		
		//added feature to set what file is analysed
		System.out.printf("The current file being used is %s.\n"
				+ "If you would like to change this, "
				+ "type in the location of the new file you would like to use.\n"
				+ "Otherwise, press enter: ",tq.getFileName());
		
		Scanner fileNameScanner = new Scanner(System.in);
		String newFileName = fileNameScanner.nextLine();
		
		if(!newFileName.trim().equals("")){
			File newFile = new File(newFileName);
			if(newFile.exists() && !newFile.isDirectory()) {
				tq.setFileName(newFileName);
			}else{
				System.out.println("This file doesn't exist.");
				return;
			}
		}
				
		// ENHANCEMENT #1: added option to only return lines with ALL keywords
		System.out.printf("Do you want each line to contain ALL keywords? (T/F) ");
		Scanner responseScanner = new Scanner(System.in);
		String response = responseScanner.next();
		boolean containsAll;
		if (response.toLowerCase().equals("t")){
			containsAll = true;
		}else if (response.toLowerCase().equals("f")){
			containsAll = false;
		}else{
			System.out.println("Invalid response.");
			return;
		}
		
		System.out.printf("Please enter the start and end characters of the quotes you would like to find (in order, seperated by a space): ");
		Scanner quoteSymbolsScanner = new Scanner(System.in);
		
		
		String[] symbols = new String[2];
		String[] quoteSymbolsLine = quoteSymbolsScanner.nextLine().split(" ");
		
		if(quoteSymbolsLine.length != 0){
			if(quoteSymbolsLine.length == 2){
				for(int i = 0; i < 2; i++){
					if(quoteSymbolsLine[i].length() == 1){
						symbols[i] = quoteSymbolsLine[i];
					}else{
						System.out.println("The start and end characters must be one character in length.");
						return;
					}
				}
			}else{
				System.out.println("Wrong number of symbols");
				return;
			}
		}
		
// added feature: ability to enter high and low rate
		System.out.printf("Please enter your low and high rate (in order, seperated by a space): ");
		Scanner rateScanner = new Scanner(System.in);
		
		double[] rates = new double[2];

		for(int i = 0; i < 2; i++){
			if(!rateScanner.hasNextDouble()){
				System.out.println("Invalid input.");
				return;
			}else{
				rates[i] = rateScanner.nextDouble();
			}
		}
		
		BuggyAnalyzer ba = new BuggyAnalyzer();
		
		BuggyBiller bi = new BuggyBiller(rates[0], rates[1]);
		System.out.printf("\n");
		
		List<String> lines = tq.query(containsAll, args);
		
		List<BuggyAnalysis> analysis = ba.analyze(lines);
		
		List<Double> bill = bi.bill(analysis);
		

		System.out.printf("Charge of $%.2f based on %d lines(s) containing %s keyword(s) [%s]%n",
				bill.get(0),
				analysis.get(0).getTotalLines(),
				(containsAll) ? "all":"the",
				String.join(", ", args)
				);
	
		System.out.printf("\n==========\nLINES WITH %s KEYWORD(S)\n==========\n",
				(containsAll) ? "ALL": "AT LEAST ONE");
		for(String t: lines){
			System.out.println(t);
		}
		
		//ENHANCEMENT #2: print out the count of the matches each keywords
		System.out.printf("\n==========\nKEYWORD COUNT\n==========\n");
		HashMap<String, Integer> keywordMatchHashMap = tq.countAllMatches(args);
		if(keywordMatchHashMap.size() == 0){
			System.out.println("there were no matches");
		}else{
			for (String word: keywordMatchHashMap.keySet()){
				System.out.println(word  +" - "+ keywordMatchHashMap.get(word));
			}
		}
		System.out.println("\nNOTE: if you selected to show only lines with ALL keywords, \n"
				+ "the keyword count may not be consistent with the lines shown as lines "
				+ "with at least one keyword are omitted.\n");
		
		//ENHANCEMENT #3: return quotes within a start and end character
		System.out.println("==========\nQUOTES\n==========");
		// added feature: ability to enter high and low rate

		
		List<String> quotes = tq.quoteSubstrings(symbols[0], symbols[1]);
		
		if(quotes.size() == 0){
			System.out.println("No matched quotes");
		}else{
			for(String t: quotes){
				System.out.println(t);
			}
		}
	}
}
