package ryze.analytics;

import java.util.LinkedList;
import java.util.List;

public class BuggyAnalyzer implements Analyzer<String, BuggyAnalysis>{

	@Override
	public List<BuggyAnalysis> analyze(List<String> lines) {
		List<BuggyAnalysis> res = new LinkedList<>();

		BuggyAnalysis lysis = new BuggyAnalysis();
		res.add(lysis);

		lysis.setTotalLines(calcTotalLines(lines));
		lysis.setTotalWords(calcTotalWords(lines));
		lysis.setMinLineLength(calcMinLineLength(lines));
		lysis.setMaxLineLength(calcMaxLineLength(lines));
		lysis.setAvgLineLength(calcAvgLineLength(lines));
		return res;
	}

	private double calcAvgLineLength(List<String> lines) {
		// bug: can't divide by 0
		if(calcTotalLines(lines) == 0){
			return 0;
		}
		// bug: int/int doesn't return double
		return calcTotalWords(lines)/(double) calcTotalLines(lines);
	}
	
	//added feature: calculate length of individual line
	private int calcLineLength(List<String> lines, int index) {
		String line = lines.get(index);
		String[] words = line.split(" ");
		int lineLen = 0;
		for(int j=0; j<words.length; j++){
			lineLen++;
		}
		return lineLen;
	}

	private int calcMinLineLength(List<String> lines) {
		if(calcTotalLines(lines) == 0){
			return 0;
		}
		// previously minLineLen started at 0 meaning it was impossible to lower than it
		int minLineLen = calcLineLength(lines, 0);
		for(int i=1; i<lines.size(); i++){
			int lineLen = calcLineLength(lines, i);
			if(lineLen < minLineLen ) minLineLen = lineLen;
		}
		return minLineLen;
	}
	
	private int calcMaxLineLength(List<String> lines) {
		if(calcTotalLines(lines) == 0){
			return 0;
		}
		int maxLineLen = calcLineLength(lines, 0);
		for(int i=1; i<lines.size(); i++){
			int lineLen = calcLineLength(lines, i);
			if(lineLen > maxLineLen ) maxLineLen = lineLen;
		}
		return maxLineLen;
	}
	
	private int calcTotalWords(List<String> lines) {
		int wordcount = 0;
		// i starts at 0
		for(int i=0; i<lines.size(); i++){
			String line = lines.get(i);
			String[] words = line.split(" ");
			// j starts at 0
			for(int j=0; j<words.length; j++){
				wordcount = wordcount + 1;
			}
		}
		return wordcount;
	}

	private int calcTotalLines(List<String> lines) {
		int count = 0;
		// i starts at 0
		for(int i=0; i<lines.size();i++){
			count = count + 1;
		}
		return count;
	}

}
