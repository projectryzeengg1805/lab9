package ryze.query;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class BuggyFileTextQuery extends SmallFileTextQuery {
// changed fileTextQuery
	@Override
	boolean contains(boolean containsAll,String line, String... keywords){
		// bug: starts with //
		if(line.trim().startsWith("//")) return false;
		
		for(String word: keywords){
			Pattern ptrn = Pattern.compile(word);
			Matcher m = ptrn.matcher(line);
			if(!(containsAll) && m.find()) return true;
			if(containsAll && !(m.find())) return false;
		}
		if(containsAll){
			return true;
		}
		return false;
	}

}
