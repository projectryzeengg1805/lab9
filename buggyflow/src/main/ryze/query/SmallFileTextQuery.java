package ryze.query;

public class SmallFileTextQuery extends FileTextQuery{
	
	public static final String DEFAULT_FILE = "data/cat_and_mat.txt";

	public SmallFileTextQuery(){
		super();
		super.setFileName(DEFAULT_FILE);
	}
}
