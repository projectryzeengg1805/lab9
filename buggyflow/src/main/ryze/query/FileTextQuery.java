package ryze.query;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

//added
import java.util.HashMap;

/**
 * Reads lines of text from a file AS-IS and returns those with matching keywords.
 * @author whwong
 *
 */
public class FileTextQuery implements Query<String>{

	private static Logger LOG = Logger.getLogger(FileTextQuery.class.getName());
	
	public static final String DEFAULT_FILE = "data/war_and_peace.txt";
	
	private String fileName =  DEFAULT_FILE;
	
	private List<String> lines;
	
	public FileTextQuery(){
		loadFile();
	}
	
	public FileTextQuery(String file){
		setFileName(file);
	}
	
	private void loadFile(){
		try (Scanner scanner = new Scanner(new File(fileName))){
			LinkedList<String> lines = new LinkedList<>();
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				lines.add(line);
			}
			
			this.lines = lines;
			
			
		} catch (FileNotFoundException e) {
			LOG.log(Level.SEVERE, fileName + " cannot be found.",  e);
		}
	}
	
	public void setFileName(String name){
		fileName = name;
		loadFile();
	}
	
	public String getFileName(){
		return fileName;
	}
	
	@Override
	public List<String> query(String... keywords) {
		if(keywords == null || keywords.length == 0){
			return lines.stream().collect(Collectors.toList());
		}else{
			return lines.stream()
					.filter(s -> contains(false, s, keywords))
					.collect(Collectors.toList());
		}
	}
	
	public List<String> query(boolean containsAll, String... keywords) {
		if(keywords == null || keywords.length == 0){
			return lines.stream().collect(Collectors.toList());
		}else{
			return lines.stream()
					.filter(s -> contains(containsAll, s, keywords))
					.collect(Collectors.toList());
		}
	}

	boolean contains(boolean containsAll, String line, String... keywords){
		for(String word: keywords){
			Pattern ptrn = Pattern.compile("\\b"+word+"\\b", Pattern.CASE_INSENSITIVE);
			Matcher m = ptrn.matcher(line);
			if(m.find()) return true;
		}
		return false;
	}
	

	
	// added method: counts the number of matches per line;
	int countLineMatches(String line, String... keywords){
		int counter = 0;
		if(line.trim().startsWith("//")) return 0;

		
		return counter;
	}
	
	// takes in at at least 0 strings as its parameter
	public HashMap<String, Integer> countAllMatches(String... keywords){
		// a count of all keywords is stored in a hashmap. the index is the keyword, the value is the count
		HashMap<String, Integer> keywordMatchCount = new HashMap<String, Integer>();
		// if there is no keyword, we return an empty hashmap
		if(keywords == null || keywords.length == 0){
			return new HashMap<String, Integer>();
		}else{
			// lines (in line 118) refers to the lines in the text file
			// we look through the each line
			for (int i = 0; i < lines.size(); i++){
				// for each line, we look at each keyword
				for(String word: keywords){
					Pattern ptrn = Pattern.compile(word);
					Matcher m = ptrn.matcher(lines.get(i));
					// then if we find a match, we update our hashmap
					while (m.find()){
						// if the word is already in the hashmap, we get the count and add 1 to it
						if(keywordMatchCount.containsKey(word)){
							int currentCount = keywordMatchCount.get(word);
							keywordMatchCount.put(word, currentCount + 1);
						// otherwise, we set the count to 1
						}else{
							keywordMatchCount.put(word, 1);
						}
					}
				}
			}
		}
		// finally, we return the hashmap
		return keywordMatchCount;
	}
	
	public List<String> quoteSubstrings(String startSymbol, String endSymbol){
		String joinedLines = String.join(" ", lines);
		String[] joinedLinesChars = joinedLines.split("");
		List<String> quotes = new LinkedList<String>();
		boolean quoteOpened = false;
		
		int quoteStartIndex = 0;
		int quoteEndIndex = 0;
		for(int i = 0; i < joinedLinesChars.length; i++){
			if(!quoteOpened && joinedLinesChars[i].equals(startSymbol)){
				quoteStartIndex = i;
				quoteOpened = true;
			}else if (quoteOpened && joinedLinesChars[i].equals(endSymbol)){
				quoteEndIndex = i;
				quoteOpened = false;
				// add quote to quotes array
				quotes.add(joinedLines.substring(quoteStartIndex + 1, quoteEndIndex));
			}
		}
		
		return quotes;
	}
}
