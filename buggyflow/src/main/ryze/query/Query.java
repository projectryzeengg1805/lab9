package ryze.query;

import java.util.List;

public interface Query<T>{

	public List<T> query(String... keywords);
	
}
