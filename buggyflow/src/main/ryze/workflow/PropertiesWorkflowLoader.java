package ryze.workflow;

import java.io.File;
import java.io.FileReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class PropertiesWorkflowLoader {

	public static String PROP_NAME = "workflows.location"; //property name pointing to location of workflow files
	public static String WFL_DIR = "workflows"; //default directory of workflows
	
	@SuppressWarnings("rawtypes")
	public static List<WorkflowInfo> loadWorkflows() throws Exception{
		
		List<WorkflowInfo> res = new LinkedList<>();
		String wflDir = System.getProperty(PROP_NAME, WFL_DIR);
		
		File dir = new File(wflDir);
		if(dir.exists() && dir.isDirectory()){
			
			File[] rwfFiles = dir.listFiles(f -> f.getName().endsWith(".rwf"));
			for(File f : rwfFiles){
				Properties p = new Properties();
				p.load(new FileReader(f));
				PropertiesWorkflow wf = new PropertiesWorkflow(p);
				String wfName = f.getName().substring(0, f.getName().lastIndexOf(".rwf"));
				
				Date dLast = new Date(f.lastModified());
				res.add(new WorkflowInfo(wfName, dLast, wf));
			}
		}else{
			throw new Exception("Workflow directory " + dir.getAbsolutePath() + " not found.");
		}
		
		return res;
	}
}
