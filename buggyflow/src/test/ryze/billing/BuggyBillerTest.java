package ryze.billing;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import ryze.analytics.BuggyAnalysis;

public class BuggyBillerTest {

	@Test
	public void buggybiller_allZeroAnalysis_0(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(0,0,0,0,0.0);
		assertEquals(0.0, bb.bill( Arrays.asList(lysis)).get(0), 0.01 );
	}

	@Test
	public void buggybiller_lowerRate_resultExpected(){ //TODO change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		
		BuggyAnalysis lysis = new BuggyAnalysis(10,55,10,1,5.5);
		assertEquals(165, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
		
	}
	
	
	@Test
	public void buggybiller_lowerRateOddNumberOfLines_resultExpected(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(7,23,8,1,3.285714);
		assertEquals(69, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
	
	@Test
	public void buggybiller_lowerRateSameRate_resultExpected(){
		BuggyBiller bb = new BuggyBiller(2, 2);
		BuggyAnalysis lysis = new BuggyAnalysis(7,23,8,1,3.285714);
		assertEquals(46, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
	
	@Test
	public void buggybiller_higherRate_resultExpected() {//TODO change test name
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(10,58,10,1,5.8);
		assertEquals(406, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
	
	@Test
	public void buggybiller_higherRateOddNumberOfLines_resultExpected(){
		BuggyBiller bb = new BuggyBiller(3,7);
		BuggyAnalysis lysis = new BuggyAnalysis(5,21,5,3,4.2);
		assertEquals(147, bb.bill(Arrays.asList(lysis)).get(0), 0.01);
	}
}
