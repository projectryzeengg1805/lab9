package ryze.analytics;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class BuggyAnalyzerTest {

	
	@Test
	public void buggyanalyzer_nolines_all0inbuggyanalysis(){
		List<String> noLines = new  LinkedList<>(); //here is an example on how to create input corresponding to no input lines
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(noLines).get(0);
		assertEquals(new BuggyAnalysis(0,0,0,0,0.0), lysis);
	}
	
	@Test
	public void buggyanalyzer_threeLines_totalLinesIs3(){
		List<String> threeLines = Arrays.asList(new String[]{"hello", "beautiful", "world"}); // an example of short input
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(threeLines).get(0);
		assertEquals(3, lysis.getTotalLines());
	}

	@Test
	public void buggyanalyzer_multLines_totalLineIs7(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times, it was the worst",
				"of times",
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		// added:
		assertEquals(7, lysis.getTotalLines());
	}
	
	// ? what is wordDelimitation, what is the difference between wordCount and totalwordCount ?

	@Test
	public void buggyanalyzer_wordDelimitation_resultExpected(){ //TODO change test name
		List<String> wordDelimitation = Arrays.asList(new String[]{
				"my", "word-count", "test"
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordDelimitation).get(0); 
		assertEquals(3, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_wordCount_wordCountIs4(){ //TODO change test name
		//TODO Construct your own data 
		List<String> wordCount = Arrays.asList(new String[]{
				//added:
				"my", "word count", "test"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(wordCount).get(0);
		//added:
		assertEquals(4, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_totalWordCount_wordCountIs5(){ //TODO change test name
		//TODO Construct your own data 
		List<String> totalWordCount = Arrays.asList(new String[]{
				//added:
				"my", "second word count", "test"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(totalWordCount).get(0); 
		//added:
		assertEquals(5, lysis.getTotalWords());
	}

	@Test
	public void buggyanalyzer_avgLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> avgLineLen = Arrays.asList(new String[]{
				"my", "average", "line length", "test"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(avgLineLen).get(0); 
		//TODO you guys to do this
		assertEquals( 
			5/(double)4, // TODO change to your_expected_value> 
			lysis.getAvgLineLength(),
			0.01 // we accept an error 0.01 and less
		);
	}
	@Test
	public void buggyanalyzer_minLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> minLineLen= Arrays.asList(new String[]{
				"my", "minimum", "line length", "test"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(minLineLen).get(0); 
		assertEquals(1, lysis.getMinLineLength());
	}
	@Test
	public void buggyanalyzer_maxLineLength_resultExpected(){ //TODO change test name
		//TODO Construct your own data 
		List<String> maxLineLen= Arrays.asList(new String[]{
				"my", "maximum", "line length", "test"
		});
		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(maxLineLen).get(0); 
		assertEquals(2, lysis.getMaxLineLength());
	}
	@Test
	public void buggyanalyzer_multLines_totalLineIs7_all_Variables_considered(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"times,",
				"times",
				"times",
				"times hello",
				"times hello"
				
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		System.out.println(lysis.getMaxLineLength());
		assertEquals(new BuggyAnalysis(10,12,2,1,1.2), lysis);
}
	@Test
	public void buggyanalyzer_multLines_totalLineIs7_all_Variables_random(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"was",
				"the",
				"best",
				"of",
				"ti\nmes,",
				"times",
				"times",
				"times",
				"times"
				
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		System.out.println(lysis.getMaxLineLength());
		assertEquals(new BuggyAnalysis(10,10,1,1,1), lysis);
}
	@Test
	public void buggyanalyzer_multLines_commented_lines(){ //TODO change test name
		List<String> multLines = Arrays.asList(new String[]{ // an example of longer input
				"it",
				"//hello",
				"the",
				"//hello",
				"of",
				"times,",
				"times",
				"times",
				
		});

		BuggyAnalyzer ba = new BuggyAnalyzer();
		BuggyAnalysis lysis = ba.analyze(multLines).get(0);
		System.out.println(lysis.getMaxLineLength());
		assertEquals(new BuggyAnalysis(8,8,1,1,1), lysis);
	
	
	}
	
}